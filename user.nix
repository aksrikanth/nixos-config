{ pkgs, ...}: {

  users = {
    users = {
      srikanth = {
        isNormalUser = true;
        home = "/home/srikanth";
        description = "Srikanth Agaram";
        extraGroups = ["wheel" "networkmanager"];
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDq25ZhOaVE3HuQKj1+CX6sPsmvTeb+HltYyJ2llUjkReYhrwyB7s9TTIQ0myq3VCUf8YJUBCvc6nGZ1wELXE5RsM/kxkOs8l8gnRXHKA/8NNipN10AMDTVdB8AvyRQILjDKYQPNaVDYeiU1WDO5krkxKeHyYt734wdyj4qfnSa+Po+29sTVhRXCE+39MUr3ln57cCXMDefZHNSd1gxbiCMiWeQQjulPedRehxS/9uNde5oVbIhuEKf5fM8Io8ozgYu4868S1XeHx5QIzTLIUvRUi4l+4c5fF4YyTFTXcN/eB1yzatfUtJUsCn/bC/AGjawobZJ8mAvO97qnE0n7zypDMUk4wyiP7xad9AIOnuMc1dDlDaDNI7EKfOBpSvU36FfHf0e/3MzgY8FVeRhREkMP1fDcrQOrP4cCfypDq53cIq4oSr+xlYarD85utIUf2RhH3uqKxfMqP6yjCOYz6SdMS0taVN1vg4ICWopYnE2WsS697NSJUYYsLgKPn2+Da8QjFpEgbaz7YJCD714c+5eHVRwsC4IpXAfH3VWmO8+R3t7pFW0+ZCfRMf2YOOj0zKYhmdS0u/ujqr8C6VeaHBFqSQ9XNpS2gliC9awdblrmgFRCCtnrgHA610yPpBBCBejMtOQ1zzd7EnBGuCsRkD1pursvVbFNfSuf8LOnMLEWw== sagaram@ebay.com"
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDjyF51pHCciQcKfJ3jvTKNXBmw5YQP7cBs9PojShbZD8g68fYMy8keIPCRsDCvhuiwtRmZU5eO4HGOcJ+mzW6O/2eSgV95mYQX1/MwQRMwRub6OVv+33t3rJosV0HrJak3/TDFfHhxZwZuPcrEy0lRlnb+xSdHqcHYVFd6MLrP+vOTyPJ8qUzPXrdLBI8UbBwhf+m9PisWDWh/WqmWUixkbEI7iHm4n2xHWcUmDjpKQGm2ibO2MK8BXwfurUF1QybZ4kh+NF6AM/W4Clt4zZty/AxeElXxnPm/XCmgvZKItoYdDWQevmLwn7SVuoHCkGTILenzn8pyFrZGbqKSPd54EI4J6ASsHAbOwqG8etKPPoEy86ZazoJax64B4a50MIb3znoQ9Vd6K9l8+Vs0Lp5cZtmyt2zHF37Hyk9c9NOJHS6NwFR5pvX5e+7MYhZWk2HnJIUVfbaVJ+6UAdJNVehKbTZEXBHUxDRIYcqkmskqbjxGb4l2/dKqyU4QeIj0jUdmJYZJZ3A2DytBAkZ0DSA2toIbufrSK0S3fOimsqDv+HrAL/Bt0F70P4ohx7aMhVK5wySlwmmzjEqKpgBg+iUlpA07bRHh58CLB4k4QQZTN9awHF9J+p3yHpWRSfHkCNALbY8hkFCl+R+1eRFAhSUEg/Nk4tWC/o8ojoyGbkVJXw== ak@srikanthak.name"
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDErkDvkZGO90AZtatv7lgeNWGKPh9d6gubYEa2FZw96YGVPOdP6Xv8raxkFp/1PTfQMdMufHPzX4wmXaiygoldohIhvrE/ACKtiUv0f1Qi9fb6g5iNA7O5+7GIKNf7sv7+nNPr57LewE5Zh/Y3739TQb7v7PA56/ce4XJmqCj8WDclXq1zI8FotX4sm9MEm2OET+ikzR1iSFwJ9hHmPXx07Y6hPDZuWACb1V3AV2jehb9NXSf1khEdawts00ZUJXEI+H0WiYs3YP3RccA5m0vTJveX2lPjjtRqtiTTosDuQUqMPbkNbQhILn2KjD+DVtQkiA6aw8sbEg87r1pYL3uxz54MGL9LwczQEBr8iFUM4h1MekA52pbKlOeYDgIXwlWzaB0OzVOP2I97vd2WjpFVjITbXW1Dn+oXtJfzlQLivc6FiYmlbSz/77tBGYDblJF6uEhdmjdonpzYHKvq+JbR6Bq6JcSjSQC5Utbog5VHXtULi7f9cCZL+q685VxqxgsQfyuvWQhnsv9e7lQCiHGZC7jGqPUdocjOvwxxZ7fAu3sfd7T7GMQYFju9xfQDFHd4cHkb1On6KxptYNKzfZujdMjDocR9PrmycGufOcHvNKQmAogmPJaDRuQ5YyLTqH0qE8bwXm3ixfzHmAJUKhN5kNC18btaQLu+KegQE5ORnQ== srikanth@nixos"
        ];
      };
    };
  };
}
