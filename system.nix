{ pkgs, ... }: {
  imports = [
    ./user.nix
    ./local.nix
  ];

  environment = {
    systemPackages = with pkgs; [
      axel
      elinks
      graphicsmagick
      git
      htop
      lynx
      nginx
      ruby
      sqlite
      tig
      tmux
      vim
      wget
    ];
  };

  boot = {
    cleanTmpDir = true;
  };

  networking = {
    firewall = {
      allowPing = true;
    };
  };

  services = {
    openssh = {
      enable = true;
      ports = [ 10022 ];
    };
  };
}
